import React from "react";
class Fetch extends React.Component {
  constructor() {
    super();
    this.state = {
      users: null,
    };
  }
  componentDidMount() {
    fetch("https://reqres.in/api/users").then((resp) => {
      resp.json().then((result) => {
        this.setState({users:result.data});
      });
    });
  }
  render() {
    return (
      <div>
        <h1>data</h1>
        {this.state.users
          ? this.state.users.map((items, i) => (
              <div>
                <p>{i}--{items.first_name}---{items.email}</p>
              </div>
            ))
          : null}
      </div>
    );
  }
}
export default Fetch;
