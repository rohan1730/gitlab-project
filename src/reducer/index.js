import change from './upDown';
import {combineReducers} from "redux";

const rootReducer=combineReducers({
    change
})

export default rootReducer;